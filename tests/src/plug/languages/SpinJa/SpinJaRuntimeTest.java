package plug.languages.SpinJa;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.explorer.AbstractExplorer;
import plug.explorer.BFSExplorer;
import plug.explorer.ConcurrentBFSExplorer;
import plug.explorer.DFSExplorer;
import plug.language.SpinJa.SpinJaPlugin;
import plug.language.SpinJa.runtime.SpinJaTransitionRelation;
import plug.statespace.SimpleStateSpaceManager;


import static org.junit.Assert.assertNotSame;

public class SpinJaRuntimeTest {
    protected final ILanguagePlugin module = new SpinJaPlugin();

    protected SpinJaTransitionRelation load(String fileName)  throws Exception {
        return (SpinJaTransitionRelation) module.getLoader().getRuntime(fileName);
    }

    protected IStateSpaceManager createStateSpaceManager() {
        IStateSpaceManager stateSpaceManager = new SimpleStateSpaceManager<>();
        stateSpaceManager.fullConfigurationStorage();
        stateSpaceManager.countingTransitionStorage();
        return stateSpaceManager;
    }

    public void assertBFS(String filename, int expectedNumberOfStates) throws Exception {
        SpinJaTransitionRelation runtime = load(filename);
        AbstractExplorer explorer = new BFSExplorer(runtime, createStateSpaceManager());
        explorer.execute();

        Assert.assertEquals(expectedNumberOfStates, explorer.getStateSpaceManager().size());
    }

    public void assertConcurrentBFS(String filename, int expectedNumberOfStates) throws Exception {
        SpinJaTransitionRelation runtime = load(filename);
        AbstractExplorer explorer = new ConcurrentBFSExplorer(runtime, createStateSpaceManager(), 8);
        explorer.execute();

        Assert.assertEquals(expectedNumberOfStates, explorer.getStateSpaceManager().size());
    }

    public void assertDFS(String filename, int expectedNumberOfStates) throws Exception {
        SpinJaTransitionRelation runtime = load(filename);
        AbstractExplorer explorer = new DFSExplorer(runtime, createStateSpaceManager());
        explorer.execute();

        Assert.assertEquals(expectedNumberOfStates, explorer.getStateSpaceManager().size());
    }

    @Test
    public void testDifferentRuntimeViews() {
        IRuntimeView view1 = module.getRuntimeView(new SpinJaTransitionRelation());
        IRuntimeView view2 = module.getRuntimeView(new SpinJaTransitionRelation());
        assertNotSame(view1, view2);
    }

    @Test
    public void testInitializeRuntime() throws Exception {

        ITransitionRelation runtime = load("tests/resources/adding.1.pm");

        assert(((SpinJaTransitionRelation)runtime).getModel() != null);
    }

    @Test
    public void testAdding1_bfs() throws Exception {
        assertBFS("tests/resources/adding.1.pm", 7372);
    }


    public void testPeterson4_sumo_bfs() throws Exception {
        assertBFS("tests/resources/bench-07-peterson4.sumo", 7372);
    }

    @Test
    public void testAdding1_dfs() throws Exception {
        assertDFS("tests/resources/adding.1.pm", 7372);
    }

    @Test(expected=RuntimeException.class)
    public void testAdding1_concurrentbfs() throws Exception {
        assertConcurrentBFS( "tests/resources/adding.1.pm", 7372);
    }

    @Test @Ignore
    public void testAnderson2_bfs() throws Exception {
        assertBFS( "tests/resources/anderson.2.pm", 1459);
    }

    @Test(expected=RuntimeException.class)
    public void testAnderson2_concurrentbfs() throws Exception {
        assertConcurrentBFS( "tests/resources/anderson.2.pm", 1459);
    }

    @Test @Ignore
    public void testAt1_bfs() throws Exception {
        assertBFS("tests/resources/at.1.pm", 1459);
    }

    @Test @Ignore
    public void testAt1_dfs() throws Exception {
        assertDFS("tests/resources/at.1.pm", 1459);
    }

    @Test(expected=RuntimeException.class)
    public void testAt1_concurrentbfs() throws Exception {
        assertConcurrentBFS("tests/resources/at.1.pm", 1459);
    }

    @After
    public void removeFiles() throws IOException {
        File tmp = new File("tmp");
        if (!tmp.exists()) return;
        for(Path p : Files.walk(tmp.toPath(), FileVisitOption.FOLLOW_LINKS).
                sorted(Comparator.reverseOrder()). // reverse; files before dirs
                toArray(Path[]::new))
        {
            Files.delete(p);
        }
    }

}
