package plug.language.SpinJa;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;
import plug.language.SpinJa.runtime.SpinJaConfiguration;
import plug.language.SpinJa.runtime.SpinJaTransitionRelation;
import plug.utils.Pair;
import spinja.concurrent.model.ConcurrentModel;
import spinja.promela.model.PromelaTransition;
import spinja.util.ByteArrayStorage;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class SpinJaRuntimeView implements IRuntimeView<SpinJaConfiguration, Pair<PromelaTransition, SpinJaConfiguration>> {

    SpinJaTransitionRelation runtime;

    public SpinJaRuntimeView(SpinJaTransitionRelation runtime) {
        this.runtime = runtime;
    }

    @Override
    public SpinJaTransitionRelation getRuntime() {
        return runtime;
    }

    @Override
    public List<ConfigurationItem> getConfigurationItems(SpinJaConfiguration value) {
        ConcurrentModel<PromelaTransition> model = runtime.getModel();
        synchronized (model) {
            ByteArrayStorage reader = new ByteArrayStorage();
            reader.setBuffer(value.state);
            model.decode(reader);

            List<ConfigurationItem> children = new ArrayList<>();
            children.add(getTreeForBehavior(model));
            for (int i = 0; i<model.getNrProcesses(); i++) {
                ConfigurationItem childTree = getTreeForBehavior(model.getProcess(i));
                children.add(childTree);
            }

            return children;
        }
    }

    ConfigurationItem getTreeForBehavior(Object bhc) {
        List<ConfigurationItem> children = new ArrayList<>();
        for (Field field : bhc.getClass().getDeclaredFields()) {
            ConfigurationItem fieldTree = getTreeForField(bhc, field);
            if (fieldTree != null) {
                children.add(fieldTree);
            }
        }

        for (Field field : bhc.getClass().getSuperclass().getDeclaredFields()) {
            if (field.getName().startsWith("_sid")) {
                ConfigurationItem fieldTree = getTreeForField(bhc, field);
                if (fieldTree != null) {
                    children.add(fieldTree);
                }
            }
        }

        ConfigurationItem parentTree = new ConfigurationItem("process", bhc.getClass().getSimpleName(), null, children);
        parentTree.setExpanded(true);
        return parentTree;
    }

    ConfigurationItem getTreeForField(Object object, Field field) {
        try {
            field.setAccessible(true);
            Object fVal = field.get(object);

            Object value;
            if (field.getName().startsWith("this")) { return null; }
            else if (field.getType().isArray()) {
                StringBuilder b = new StringBuilder();

                int iMax = Array.getLength(fVal) - 1;
                if (iMax == -1)
                    value = "[]";
                else {
                    b.append('[');
                    for (int i = 0; ; i++) {
                        b.append(Array.get(fVal, i));
                        if (i == (Array.getLength(fVal) - 1)) {
                            b.append(']');
                            break;
                        }
                        b.append(", ");
                    }
                    value = b.toString();
                }
            } else if (field.getType().isArray()) {
                value = Arrays.deepToString((Object[])fVal);
            } else {
                value = fVal.toString();
            }

            ConfigurationItem varField = new ConfigurationItem("variable", field.getName() + " = " + value, null, null);
            return varField;
        } catch (IllegalAccessException e) {
            //e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getFireableTransitionDescription(Pair<PromelaTransition, SpinJaConfiguration> transition) {
        return null;
    }

}
