package plug.language.SpinJa.runtime;

import plug.core.defaults.DefaultConfiguration;

import java.util.Arrays;

/**
 * Created by ciprian on 25/04/15.
 * This class is strange. Its existence is motivated by the need of the JoMC language of a Configuration class.
 * Also the Plug has a configuration class but it is not the same.
 * So this class bridges the gap.
 * It is better to have it extends Configuration instead than compose it since many instances will be present (a pointer is to expensive here).
 */
public class SpinJaConfiguration extends DefaultConfiguration<SpinJaConfiguration> {

    public byte[] state;

    public SpinJaConfiguration() {

    }

    @Override
    public SpinJaConfiguration createCopy() {
        SpinJaConfiguration newC = new SpinJaConfiguration();

        newC.state = Arrays.copyOf(state, state.length);
        return newC;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(state);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if(obj instanceof SpinJaConfiguration) {
            byte[] otherState = ((SpinJaConfiguration)obj).state;
            return Arrays.equals(state, otherState);
        }
        return false;
    }

    @Override
    public String toString() {
        return "SpinJaConfig [" + state.toString() + "]";
    }
}
