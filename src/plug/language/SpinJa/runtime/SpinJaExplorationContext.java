package plug.language.SpinJa.runtime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import org.eclipse.jdt.core.compiler.batch.BatchCompiler;
import plug.language.SpinJa.compiler.SpinJaCompiler;
import spinja.concurrent.model.ConcurrentModel;
import spinja.promela.compiler.Specification;
import spinja.promela.model.PromelaTransition;

public class SpinJaExplorationContext {
    ConcurrentModel<PromelaTransition> theModel;

    public synchronized ConcurrentModel<PromelaTransition> initializeRuntime(File programFile, String generatedOutputPath) throws Exception {
        ConcurrentModel<PromelaTransition> model = null;

        if (!programFile.exists() || !programFile.isFile()) {
            throw new IOException("File " + programFile.getName() + " does not exist or is not a valid file!");
        }

        final Specification spec =
                SpinJaCompiler.compile(programFile, "Pan", false, false);
        String packageName = getIdentifier(programFile.getName()) + "_spinja";

        File outputDir = new File(generatedOutputPath, packageName + "/java");
        if (!outputDir.exists() && !outputDir.mkdirs()) {
            throw new Error("Error: could not generate directory " + outputDir.getName());
        }
        //write the generated code
        final File javaFile = new File(outputDir, "Pan" + "Model.java");

        final FileOutputStream fos = new FileOutputStream(javaFile);

        String modelString = spec.generateModel().replaceFirst("package spinja;", "package " + packageName + ";");

        fos.write(modelString.getBytes());
        fos.flush();
        fos.close();
        System.out.println("Written Java files for '" + programFile + "' to\n" + outputDir);

        File programPath = compileJava(programFile, "spinja", "java", "class", generatedOutputPath, new PrintWriter(System.out), new PrintWriter(System.err));
        model = loadProgram(packageName + ".PanModel", programPath);
        if (model == null) throw new Error("Could not load program");

        theModel = model;
        return model;

    }

    public String getIdentifier(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {

            if (Character.isAlphabetic(str.charAt(i)))
                sb.append(str.charAt(i));
            else
                sb.append((int) str.charAt(i));
        }
        return sb.toString();
    }

    private String getJavaByteCodeVersion() {
        return System.getProperty("java.version").substring(0, 3);
    }

    public File compileJava(
            File promFile,
            String dirExtension,
            String javaDirName,
            String classDirName,
            String generatedOutputPath,
            PrintWriter out,
            PrintWriter err) {
        final String baseName = getIdentifier(promFile.getName());

        final File outputDirectory = new File(generatedOutputPath, baseName + "_" + dirExtension).getAbsoluteFile();
        final File javaDirectory = new File(outputDirectory, javaDirName);
        final File classDirectory = new File(outputDirectory, classDirName);

        String[] command = new String[]{
                "-" + getJavaByteCodeVersion(),
                "-d", classDirectory.getPath(),
                javaDirectory.getPath()
        };

        StringBuilder commandToPrint = new StringBuilder();
        commandToPrint.append("Exec: [ecj ");
        for (String arg : command) {
            if (commandToPrint.length() > 1) commandToPrint.append(" ");
            commandToPrint.append(arg);
        }
        commandToPrint.append("]\n");
        out.println(commandToPrint.toString());

        boolean succeeded = BatchCompiler.compile(command, out, err, null);

        if (succeeded == false) {
            throw new Error("Java Compilation failed.");
        }

        return classDirectory;
    }


    public ConcurrentModel<PromelaTransition> loadProgram(String programClassName, File programPath) throws IOException {
        // loads program class
        Class<? extends ConcurrentModel<PromelaTransition>> programClass;
        try {
            Class<?> loadedClass = getClassLoader(programPath).loadClass(programClassName);
            assert (loadedClass != null);
            if (!ConcurrentModel.class.isAssignableFrom(loadedClass)) {
                throw new IllegalArgumentException("Class '" + programClassName + "' isn't an Explorer program.");
            }
            @SuppressWarnings("unchecked")
            Class<? extends ConcurrentModel<PromelaTransition>> localProgramClass = (Class<? extends ConcurrentModel<PromelaTransition>>) loadedClass;
            programClass = localProgramClass;

        } catch (ClassNotFoundException e) {
            throw new IOException("Can't find program class '" + programClassName + " in '" + programPath.getPath() + "'.");
        }

        // instantiates program
        try {
            return programClass.newInstance();
        } catch (Exception e) {
            throw new IOException("Can't load program '" + programClass.getCanonicalName() + "'.");
        }
    }

    public URLClassLoader getClassLoader(File programPath) throws IOException {

        try {
            final URL url = programPath.toURI().toURL();
            return new URLClassLoader(new URL[]{url});
        } catch (MalformedURLException e) {
            throw new IOException("Invalid program path '" + programPath.getPath() + "'.");
        }
    }
}
