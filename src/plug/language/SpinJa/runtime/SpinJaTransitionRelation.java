package plug.language.SpinJa.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import plug.core.IConcurrentTransitionRelation;
import plug.core.IFiredTransition;
import plug.statespace.transitions.FiredTransition;
import plug.utils.Pair;
import spinja.concurrent.model.ConcurrentModel;
import spinja.exceptions.SpinJaException;
import spinja.model.Condition;
import spinja.promela.model.PromelaTransition;
import spinja.search.TransitionCalculator;
import spinja.util.ByteArrayStorage;

public class SpinJaTransitionRelation implements
		IConcurrentTransitionRelation<SpinJaTransitionRelation, SpinJaConfiguration, Pair<PromelaTransition, SpinJaConfiguration>>
{

	SpinJaExplorationContext context;
	ConcurrentModel<PromelaTransition> model;
	TransitionCalculator<ConcurrentModel<PromelaTransition>, PromelaTransition> nextTransition = new TransitionCalculator<>();

	public ConcurrentModel<PromelaTransition> getModel() {
		return model;
	}

	public void setModel(SpinJaExplorationContext context) {
		this.context = context;
		this.model = context.theModel;
	}

	protected final ByteArrayStorage storage = new ByteArrayStorage();

	protected byte[] storeModel() {
		storage.init(model.getSize());
		model.encode(storage);
		return storage.getBuffer();
	}

	@Override
	public SpinJaTransitionRelation createCopy() {
		SpinJaTransitionRelation copy = new SpinJaTransitionRelation();
		copy.model = (ConcurrentModel<PromelaTransition>)model.clone();
		if (copy.model == null) throw new RuntimeException();
		return copy;
	}

	@Override
	public Set<SpinJaConfiguration> initialConfigurations() {
		final SpinJaConfiguration initial = new SpinJaConfiguration();
		initial.state = storeModel();

		return Collections.singleton(initial);
	}

	@Override
	public List<Pair<PromelaTransition, SpinJaConfiguration>> fireableTransitionsFrom(SpinJaConfiguration configuration) {
		SpinJaConfiguration current = configuration;

		ByteArrayStorage reader = new ByteArrayStorage();
		reader.setBuffer(current.state);
		model.decode(reader);

		List<Pair<PromelaTransition, SpinJaConfiguration>> fireables = new ArrayList<>();

		PromelaTransition next;
		PromelaTransition last = null;
		while ((next = nextTransition.next(model, last)) != null) {

			try { // Take the next transition and check for errors
				next.take();
			} catch (final SpinJaException ex) {
				System.out.println("Transition error " + ex.getMessage());
				return Collections.emptyList();
			}

			if (model.conditionHolds(Condition.SHOULD_STORE_STATE)) {

				SpinJaConfiguration nextConfig = new SpinJaConfiguration();
				nextConfig.state = storeModel();
				
				fireables.add(new Pair<>(next, nextConfig));
				next.undo();
			}

			last = next;
		}

		return fireables;
	}

	@Override
	public IFiredTransition<SpinJaConfiguration, ?> fireOneTransition(SpinJaConfiguration source, Pair<PromelaTransition, SpinJaConfiguration> transition) {
		return new FiredTransition<>(source, transition.b, transition.a);
	}

}

