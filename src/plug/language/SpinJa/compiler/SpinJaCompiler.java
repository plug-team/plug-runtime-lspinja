package plug.language.SpinJa.compiler;

import spinja.promela.compiler.Proctype;
import spinja.promela.compiler.Specification;
import spinja.promela.compiler.optimizer.*;
import spinja.promela.compiler.parser.ParseException;
import spinja.promela.compiler.parser.Promela;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class SpinJaCompiler {
	public static Specification compile(final File promFile, 
	                                     final String name,
		                                 final boolean useStateMerging,
		                                 final boolean verbose) {
		try {
			if (verbose)
				System.out.print("Start parsing " + promFile.getName() + "...");
			final Promela prom = new Promela(new FileInputStream(promFile));
			final Specification spec = prom.spec(name);
			if (verbose)
				System.out.println("done");

			if (verbose) 
				System.out.println("Optimizing graphs...");
			final GraphOptimizer[] optimizers = new GraphOptimizer[] {
					useStateMerging ? new StateMerging() : null, new RemoveUselessActions(),
					new RemoveUselessGotos(), new RenumberAll(),
			};
			for (final Proctype proc : spec) {
				if (verbose) {
					System.out.println("Initial graph for process " + proc + ":");
					System.out.println(proc.getAutomaton());
				}
				for (final GraphOptimizer opt : optimizers) {
					if (opt == null)
						continue;
					opt.optimize(proc.getAutomaton());
					if (verbose) {
						System.out.println("After " + opt.getClass().getSimpleName() + ":");
						System.out.println(proc.getAutomaton());
					}
				}
				if (verbose)
					System.out.println(proc.getAutomaton());
			}

			final Proctype never = spec.getNever();
			if (never != null) {
				if (verbose) {
					System.out.println("Initial graph for never claim:");
					System.out.println(never.getAutomaton());
				}
				for (final GraphOptimizer opt : optimizers) {
					if (opt != null) {
						opt.optimize(never.getAutomaton());
					}
					if (verbose) {
						System.out.println("After " + opt.getClass().getSimpleName() + ":");
						System.out.println(never.getAutomaton());
					}
				}
				if (verbose)
					System.out.println(never.getAutomaton());
			}
			if (verbose)
				System.out.println("Optimization done");

			return spec;
		} catch (final FileNotFoundException ex) {
			System.out.println("Promela file " + promFile.getName() + " could not be found.");
		} catch (final ParseException ex) {
			System.out.println("Parse exception in file " + promFile.getName() + ": "
								+ ex.getMessage());
		}
		return null;
	}
}