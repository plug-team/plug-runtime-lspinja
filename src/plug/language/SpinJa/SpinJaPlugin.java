package plug.language.SpinJa;

import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.language.SpinJa.runtime.SpinJaTransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class SpinJaPlugin implements ILanguagePlugin<SpinJaTransitionRelation> {
    protected final SpinJaLoader loader = new SpinJaLoader();

    @Override
    public String[] getExtensions() {
        return new String[] {".sumo", ".pm"};
    }
    @Override
    public String getName() {
        return "SpinJa";
    }

    @Override
    public SpinJaLoader getLoader() {
        return loader;
    }

    @Override
    public IRuntimeView getRuntimeView(SpinJaTransitionRelation runtime) {
        return new SpinJaRuntimeView(runtime);
    }
}
