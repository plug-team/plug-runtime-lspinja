package plug.language.SpinJa;

import java.io.File;
import java.net.URI;
import java.util.Map;
import plug.core.ILanguageLoader;
import plug.language.SpinJa.runtime.SpinJaExplorationContext;
import plug.language.SpinJa.runtime.SpinJaTransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class SpinJaLoader implements ILanguageLoader<SpinJaTransitionRelation> {
    @Override
    public SpinJaTransitionRelation getRuntime(URI modelURI, Map<String, Object> options) throws Exception {
        SpinJaExplorationContext context = new SpinJaExplorationContext();

        SpinJaTransitionRelation runtime = new SpinJaTransitionRelation();
        Object outputPath;
        if (options == null || (outputPath = options.get("outputPath"))==null) {
            context.initializeRuntime(new File(modelURI), "tmp");
            runtime.setModel(context);
            return runtime;
        }
        context.initializeRuntime(new File(modelURI), "tmp");
        runtime.setModel(context);
        return runtime;
    }


}
